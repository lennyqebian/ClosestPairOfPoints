/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package closestpairofpoints;

import java.awt.Point;
import java.util.Arrays;

/**
 *
 * @author <a href="mailto:jlquevedodia@uniminuto.edu.co">Leonardo Quevedo Diaz</a>
 */
public class ClosestPairOfPoints {
    
    protected Point closest1, closest2;
    protected double closestDistance = Double.POSITIVE_INFINITY;

    public ClosestPairOfPoints(Point[] points) throws Exception {
        int n = points.length;
        
        if (n <= 1) {
            throw new Exception("Debes ingresar al menos dos puntos");
        }
        Point[] pointsSortedByX = sortPointsByX(points.clone());
        
        if (matchingPointsExist(pointsSortedByX)) {
            return;
        }
        Point[] pointsSortedByY = pointsSortedByX.clone();
        Point[] aux = new Point[n];
        closest(pointsSortedByX, pointsSortedByY, aux, 0, n - 1);
    }

    private Point[] sortPointsByX(Point[] points) {
        Arrays.sort(points, (Point p1, Point p2) -> Double.compare(p1.getX(), p2.getX()));
        System.out.println("Arreglo ordenado por coordenadas en X: " + Arrays.toString(points));
        return points;
    }

    private boolean matchingPointsExist(Point[] points) {
        int n = points.length;
        for (int i = 0; i < n - 1; i++) {
            if (points[i].equals(points[i + 1])) {
                closestDistance = 0.0;
                closest1 = points[i];
                closest2 = points[i + 1];
                return true;
            }
        }
        return false;
    }

    private double closest(Point[] pointsSortedByX, Point[] pointsSortedByY, Point[] aux, int inf, int sup) {
        if (sup <= inf) {
            return Double.POSITIVE_INFINITY;
        }
        int midIdx = inf + (sup - inf) / 2;
        Point median = pointsSortedByX[midIdx];
        double deltaLeft = closest(pointsSortedByX, pointsSortedByY, aux, inf, sup);
        double deltaRight = closest(pointsSortedByX, pointsSortedByY, aux, midIdx + 1, sup);
        double delta = Math.min(deltaLeft, deltaRight);
    }
}
