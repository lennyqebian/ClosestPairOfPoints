/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package closestpairofpoints;

import java.awt.Point;
import java.util.Scanner;
import java.util.Arrays;

/**
 *
 * @author <a href="mailto:jlquevedodia@uniminuto.edu.co">Leonardo Quevedo Diaz</a>
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Point[] points;
        Point closest1, closest2;
        Point[] pointsSortedByX, pointsSortedByY;
        int x, y;
        double closestDistance = Double.POSITIVE_INFINITY;
        Scanner scanner = new Scanner(System.in);
        System.out.print("¿Cuántos puntos deseas comparar? > ");
        int n = scanner.nextInt();
        points = new Point[n];
        scanner.useDelimiter(",\\p{javaWhitespace}*|\\p{javaWhitespace}+");

        for (int idx = 0; idx < n; idx++) {
            System.out.print("\nCoordenadas Punto " + (idx + 1) + " (x,y) > ");
            x = scanner.nextInt();
            y = scanner.nextInt();
            points[idx] = new Point(x, y);
        }
        scanner.reset();
        System.out.println("Arreglo original: " + Arrays.toString(points));
        
        int midPosition = (n % 2 == 0) ? n / 2 : (n + 1) / 2;
        System.out.println("Punto Medio: " + midPosition);
        
        for (int i = 0; i < n - 1; i++) {
            if (pointsSortedByX[i].equals(pointsSortedByX[i + 1])) {
                closestDistance = 0.0;
                closest1 = pointsSortedByX[i];
                closest1 = pointsSortedByX[i + 1];
                break;
            }
        }
        
        pointsSortedByY = pointsSortedByX.clone();
    }

}
